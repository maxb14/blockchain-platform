ELASTIC:

geany /etc/elasticsearch/elasticsearch.yml

# Set the bind address to a specific IP (IPv4 or IPv6):
#
network.host: 10.193.203.238
#
# Set a custom port for HTTP:
#
http.port: 9200
#


run elastic as a service:

service elasticsearch start|stop|restart


-----------------------------------------------------------------------------

KIBANA:

geany /etc/kibana/kibana.yml


# Kibana is served by a back end server. This controls which port to use.
server.port: 9201

# The host to bind the server to.
server.host: "10.193.203.238"

run kibana as a service:

service kibana start|stop|restart

-----------------------------------------

check cluster status:

http://10.193.203.238:9200/_cat/health?v


------------------------------------

check nodes status:

http://10.193.203.238:9200/_cat/nodes?v


----------------------------------------

check indices status:

http://10.193.203.238:9200/_cat/indices?v



----------------------------

create index:

PUT indexname


----------------------------

delete index:

DELETE /index

----------------------------

show mapping:

GET indexname/_mapping

----------------------------

PUT /scalablockdev/_mapping/eos
{
  "eos": {
	"properties":  {
			"name": {"type":"text"},
			"config": { 
				"properties": { 
					"cpuModel":    { "type": "text" }, 
					"blockchain":     { "type": "text" }, 
					"host":      { "type": "text" },
					"nbTransac":      { "type": "integer" },
					"nbAccounts":      { "type": "integer" }
				}
			},
			"result": {
				"properties": { 
					"dateStart": { "type": "text" }, 
					"dateEnd": { "type": "text" }, 
					"timeExecution": { "type": "float" }, 
					"timePerTx": { "type": "float" }, 
					"nbTxPerSec": { "type": "float" } 
				} 
			},
			"blockInterval": {
				"properties": { 
					"blockstart": { "type": "integer" },
					"blockend": { "type": "integer" },
					"nbAverageTxPerBlock": { "type": "float" }
				}
			} 
		}	
	}
}
