// IMPORTS
const fs = require('fs');

// RESULTS ACTIONS

/**
 * 
 * RESULTS PAGE CONTROL 
 * 
 */
 
/**
 * Render of the page results.twig
 */
exports.render = function(req, res, fileresult, error) {
	fs.readdir("logs/", function(err, files) {
		if (err) {
			res.render('results', {title: 'Results',
									fileresult: fileresult,
										error: error,
											files: null});	
		} else {			
			res.render('results', {title: 'Results',
									fileresult: fileresult,
										error: error,
											files: files});	
		}
	});
}

/**
 * Display of the page results.twig or redirect to home page
 */ 
exports.results = function(req, res) {
	// If the user has already filled the config
	if (req.app.get('started')) {
		module.exports.render(req, res, null, null);
	} else {
		res.redirect('/');
	}
}

/**
 * Results file control
 */
exports.resultsFile = function(req, res) {
	var logfile = req.body.logfile;
	fs.readFile('logs/'+logfile, 'utf8', function (err, data) {
		if (err) {
			return module.exports.render(req, res, null, err);
		}
		module.exports.render(req, res, data, null);
	});	
}
