// IMPORTS
var model = require('../models/formsModel');
var eosBlockchain = require('../models/eos');
var ethBlockchain = require('../models/ethereum');
var elastic = require('../models/elastic');
var fs = require('fs');
var jsonfile = require('jsonfile');
var os = require('os');
var web3 = require('web3');
var acctest = 1;
const delay = require('delay');

// CONTROLLER : RUN TESTS ACTIONS

/**
 * 
 * RUN PAGE CONTROL 
 * 
 */
 
/**
 * Render of the page run.twig
 */
exports.render = function(req, res) {
	res.render('run', { title: 'Run scalability tests',
							blockchain: req.app.get('blockchain').toUpperCase(),
								hosts: req.app.get('hosts'),
									node: req.app.get('hosts')[0] });
}
 
/**
 * Display of the page run.twig or redirect to home page
 */ 
exports.runroot = function(req, res) {
	// If the user has already filled the config
	if (req.app.get('started')) {
		module.exports.render(req, res);
	} else {
		res.redirect('/');
	}
}

/**
 * Run test control
 */
exports.runstart = function(req, res) {
	var data = req.body;

	// We save the global variables
	req.app.set('transac', data.transac);
	
	// We call the model method for validation
	var error = model.runStart(req, res, data);
	
	// Catching form errors
	if (error != null) {
		return callbackError(error);
	}

	/**
	 * Node connected : execute callback success function
	 * Node not connected : execute callback error function
	 */
	if (req.app.get('blockchain') == "eos") {
		var getBlock = eosBlockchain.eosHostConnect(data.node);

		getBlock.then(result => {
			return callbackSuccessAction(result);
		}).catch(error => {
			console.log(error);
			return callbackError(data.node+" is not connected !");
		});	
	}
	if (req.app.get('blockchain') == "ethereum") {
		var provider = ethBlockchain.ethHostConnect(data.node);
	
		provider.eth.net.isListening().then(result => {
			return callbackSuccessAction(result);
		}).catch(e => {
			return callbackError(data.node+" is not connected !");
		});		
	}
	
	// Callback function error : display the view with errors
	function callbackError(error) {
		res.send({error: error});		
	} 
	
	// Callback function succes : run test and save logs
	function callbackSuccessAction(result) {
		var timestart, datestart, timeend, dateend, totaltime, nbTxPerSec, blockend, getBlock, blockstart, filename;
		
		// create json header
		var obj = {
			name: req.app.get('blockchain')+"_"+parseInt(data.transac),
			config: {
				cpuModel: os.cpus()[0].model,
				blockchain: req.app.get('blockchain'),
				host: data.node, 
				nbTransac: parseInt(data.transac),
				nbAccounts: req.app.get('accounts')
			}
		};
		blockstart = result.head_block_num;
		filename = req.app.get('blockchain')+"_"+parseInt(data.transac)+"_"+new Date().toISOString().split('.', 1).toString();
		timestart = new Date().getTime();
		datestart = new Date().toISOString().split('.', 1).toString();
		
		var action, from = acctest, to = acctest+1;
		for (var j = 1; j <= parseInt(data.transac) ; j++) {
			/** 
			 * Transfer money between accounts config -> Loop on numbers of accounts
			 * If 3 accounts are configured, the following tests was executed :
			 * 1) user1 to user2
			 * 2) user2 to user3 
			 * 3) user3 to user1
			 * and replay to 1) while test is not finished
			 */
			if (from == req.app.get('accounts')) {
				action = eosBlockchain.eosTransferMoney(data.node, "user"+from, "user1");
				from = 1;
				to = from++;
			} else {
				action = eosBlockchain.eosTransferMoney(data.node, "user"+from, "user"+to);
				from++;
				to++;
			}
			//eosBlockchain.eosPushAction(data.node);
		}
		
	
		/* time and date */
		timeend = new Date().getTime();
		dateend = new Date().toISOString().split('.', 1).toString();
		totaltime = ((timeend - timestart) / 1000);
		nbTxPerSec = parseInt(data.transac)/Math.abs(totaltime.toFixed(3));
		
		
		var testresult = {
			dateStart: datestart,
			dateEnd: dateend,
			timeExecution: Math.abs(totaltime.toFixed(3)),
			timePerTx: Math.abs((totaltime.toFixed(3)/data.transac).toFixed(3)),
			nbTxPerSec: Math.abs(nbTxPerSec.toFixed(3))
		};
		
		obj.result = testresult;
		
		getBlock = eosBlockchain.eosHostConnect(data.node);
		getBlock.then(result => {
			blockend = result.head_block_num;
			(async () => {
				await delay(2000); 
				exploreBlocksLogs(data.node, obj, blockstart, blockend, filename, data.transac);
			})();
		}).catch(error => {
			callbackError(error.toString());
		});
		
		res.send();
		res.end();
	}
	
	// Explore blocks logs and fix
	function exploreBlocksLogs(node, obj, blockstart, blockend, filename, nbtx) {
		var blocknum, nbBlocks, txPerBlock;
	
		// check blockstart not empty
		blocknum = eosBlockchain.eosGetBlock(node, blockstart);
		blocknum.then(result => {
			if (parseInt(result.transactions.length) == 0) {
				blockstart = blockstart+1;
			}
					
			// check blockend+1 not empty
			blocknum = eosBlockchain.eosGetBlock(node, blockend+1);
			blocknum.then(result => {
				if (parseInt(result.transactions.length) != 0) {
					blockend = blockend+1;
				}
				nbBlocks = (blockend-blockstart)+1;
				txPerBlock = nbtx/nbBlocks;
				saveLogs(obj, blockstart, blockend, filename, txPerBlock);
			}).catch(e => {
				callbackError(e);
			});
		}).catch(e => {
			callbackError(e);
		});
	} 
	
	//save logs in file & elastic database
	function saveLogs(obj, blockstart, blockend, filename, txPerBlock) {
		obj.blockInterval = {blockstart: blockstart, blockend: blockend, nbAverageTxPerBlock: txPerBlock};
		jsonfile.writeFile("logs/"+filename+".log", obj, {flag: 'a', spaces: 4, EOL: '\r\n'}, function(err) {
			if (err) {
				return callbackError(err);
			}
		});
		
		// save data in elasticsearch database
		var el = elastic.indexCount();	
		el.then(result => {
			var add = elastic.addToIndex(result.count+1, req.app.get('blockchain'), obj);
			add.then(result => {
				console.log("Logs saved in elastic ! Show on kibana at http://10.193.203.238:9201/");
			}).catch(e => {
				callbackError(e);
			});
		}).catch(e => {
			callbackError(e);
		});
	}
}
