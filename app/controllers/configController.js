// IMPORTS
var model = require('../models/formsModel');
var eosBlockchain = require('../models/eos');
var ethBlockchain = require('../models/ethereum');

// CONTROLLER : CONFIG ACTIONS

/**
 * 
 * CONFIG PAGE CONTROL 
 * 
 */

/**
 * Render of the page config.twig
 */
exports.render = function(req, res, blockchain, error) {
	res.render('config', { title: 'Network configuration',
							blockchain: blockchain.toUpperCase(),
								nodes: req.app.get('nodes'),
									accounts: req.app.get('accounts'),
										blocksize: req.app.get('blocksize'),
											timeblock: req.app.get('timeblock'),
												error: error });	
}

/**
 * Display of the page config.twig or redirect to home
 */
exports.configroot = function(req, res) {
	// If the user has already filled the config
	if (req.app.get('started')) {
		module.exports.render(req, res, req.app.get('blockchain'), null);
	} else {
		res.redirect('/');
	}	 
}

/**
 * Display of the page config.twig with blockchain name
 */ 
exports.configname = function(req, res) {
	var blockchain = req.params.name; 

	// Verification if blockchain is allowed
	if (!(req.app.get('blockchainsAllowed').indexOf(blockchain) > -1)) {
		return res.redirect('/');
	} 
	req.app.set('blockchain', blockchain);
	
	// We display the config page for the selected blockchain with all parameters
	module.exports.render(req, res, blockchain, null);
}

/**
 * Post validation for config form
 */
exports.configvalid = function(req, res, next) {
	var result = req.body, blockchain = req.params.name, hosts = [], nbacc = parseInt(result.accounts);
	
	// We save the global variables
	req.app.set('nodes', result.nodes);
	req.app.set('accounts', nbacc);
	req.app.set('blocksize', result.blocksize);
	req.app.set('timeblock', result.timeblock);

	// We call the model method for validation
	var error = model.configForm(req, res, result);

	// We display error
	if (error != null) { 
		return module.exports.render(req, res, blockchain, error);
	}
	
	// Get the hosts from each node
	for (var i = 1 ; i <= result.nodes ; i++) {
		if (result['host-'+i] == undefined) {
			return module.exports.render(req, res, blockchain, "Node undefined !");
		}
		hosts.push(result['host-'+i]);
	}
	req.app.set('hosts', hosts);

	// Select and save network file config
	req.app.set('networkfile', "networks/network"+result.nodes+".json");
	
	
	/* We determine blockchain choosen */
	
	if (req.app.get('blockchain') == "eos") {
		// Run configuration on hosts or return error
		var getBlock = eosBlockchain.eosHostConnect(hosts[0]);

		// Before starting config, check that the host is active
		getBlock.then(result => {
			// If not eos node
			if (!result.hasOwnProperty("server_version")) {
				req.app.set('started', false);
				return module.exports.render(req, res, blockchain, "This is not a node EOS ! Verify your hosts.");
			}
			eosBlockchain.eosReloadConfig(hosts[0])
			eosBlockchain.eosReloadConfig(hosts[0]);
			eosBlockchain.eosCreateAccounts(hosts[0], nbacc);
			
			req.app.set('started', true);
			res.redirect(307, '/visualize');
		}).catch(error => {
			req.app.set('started', false);
			module.exports.render(req, res, blockchain, "Impossible to generate config ! Verifiy your hosts.");
		});
	} 
	if (req.app.get('blockchain') == "ethereum") {
		var provider = ethBlockchain.ethHostConnect(hosts[0]);
	
		// test connexion of node
		provider.eth.net.isListening().then(result => {
			req.app.set('started', true);
			res.redirect(307, '/visualize');
		}).catch(e => {
			req.app.set('started', false);
			module.exports.render(req, res, blockchain, "Impossible to generate config ! Verifiy your hosts.");	
		});
	}
}
