// IMPORTS
var model = require('../models/formsModel');
var eosBlockchain = require('../models/eos');
var ethBlockchain = require('../models/ethereum');

// CONTROLLER : VISUALIZE ACTIONS
 
/**
 * 
 * VISUALIZE PAGE CONTROL 
 * 
 */
 
/**
 * Render of the page visualize.twig
 */
exports.render = function(req, res) {
	res.render('visualize', { title: 'Network visualization',
								blockchain: req.app.get('blockchain').toUpperCase(),
									accounts: req.app.get('accounts'),
										networkfile: req.app.get('networkfile'),
											hosts: req.app.get('hosts') });
} 
 
/**
 * Display of the page visualize.twig or redirect to home page
 */
exports.visualizeroot = function(req, res) {
	// If the user has already filled the config
	if (req.app.get('started')) {
		module.exports.render(req, res);
	} else {
		res.redirect('/');
	}
}

/**
 * Visualize node information
 */
exports.visualizeNode = function(req, res, next) {
	// Determined which node choosen
	var node = req.body.data, 
	hostnum = node.match(/[a-zA-Z]+|[0-9]+/g)[1], 
	host = req.app.get('hosts')[hostnum-1];

	/**
	 * Node connected : execute callback function with results
	 * Node not connected : execute callback functions with errors
	 */
	if (req.app.get('blockchain') == "eos") {
		var getBlock = eosBlockchain.eosHostConnect(host);

		getBlock.then(result => {
			callbackSuccess();
		}).catch(error => {
			callbackError();
		});
	}
	if (req.app.get('blockchain') == "ethereum") {
		var provider = ethBlockchain.ethHostConnect(host);
	
		provider.eth.net.isListening().then(result => {
			callbackSuccess();
		}).catch(e => {
			callbackError();
		});
	}

	// Callback success function, node not connected : send error
	function callbackError() {
		res.send({status: "DISCONNECTED", host: host});
	}	
	
	// Callback success function, node connected : send host information
	function callbackSuccess() {
		res.send({status: "CONNECTED ON", host: host});
	}	
}
