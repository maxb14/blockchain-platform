// IMPORTS
var model = require('../models/formsModel');
var eosBlockchain = require('../models/eos');

// CONTROLLER : BLOCK EXPLORER ACTIONS

/**
 * 
 * BLOCK EXPLORER PAGE CONTROL 
 * 
 */
 
/**
 * Render of the page block.twig
 */
exports.render = function(req, res, blocknum, blockresult, error) {
	res.render('block',{title: 'Blockchain explorer',
						blockchain: req.app.get('blockchain').toUpperCase(),
							hosts: req.app.get('hosts'),
								node: req.app.get('hosts')[0],
									numblock: blocknum, 
										blockresult: blockresult,
											error: error});	
}

/**
 * Display of the page block.twig or redirect to home page
 */ 
exports.runblock = function(req, res) {
	// If the user has already filled the config
	if (req.app.get('started')) {
		module.exports.render(req, res, null, null, null);
	} else {
		res.redirect('/');
	}
}

/**
 * Run block explorer control
 */
exports.blockresult = function(req, res) {
	var data = req.body, blocknum = data.blocknum, id = data.transacid;

	// We call the model method for form validation
	var error = model.runBlock(req, res, data);
	
	// Catching form errors
	if (error != null) {
		return callbackError(null, error);
	}
	
	// Test host connexion
	var getBlock = eosBlockchain.eosHostConnect(data.node);

	/**
	 * Node connected : execute callback success function
	 * Node not connected : execute callback error function
	 */
	getBlock.then(result => {
		callbackSuccess();
	}).catch(error => {
		callbackError(null, data.node+" is not connected !");
	});	
	
	// Callback error function : display the view with error
	function callbackError(blockresult, error) {
		module.exports.render(req, res, blocknum, blockresult, error);	
	}
	
	// Callback success function : display the view with success result or errors
	function callbackSuccess() {
		// Determines if user want showing a block result or transaction result
		if (blocknum != '') {
			var getInfo = eosBlockchain.eosGetBlock(data.node, blocknum);
		} else {
			var getInfo = eosBlockchain.eosGetTransaction(data.node, id);
		}
		
		/**
		 * Block num ok or transacid ok : callback function with json results
		 * Block num not ok or transacid ok : callback function with error
		 */
		getInfo.then(report => {
			if (report.data) {
				return callbackError(null, report.data.toString());
			}
			module.exports.render(req, res, blocknum, report, error);	
		}).catch(error => {
			callbackError(null, error.toString());
		});
	}
}
