// IMPORTS
var express = require('express');
var router = express.Router();

// Get method : Call index view "index.twig" : /
router.get('/', function(req, res, next) {
	res.render('index', { title: 'Choose blockchain' });	
});

module.exports = router;
