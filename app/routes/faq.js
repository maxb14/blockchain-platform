// IMPORTS
var express = require('express');
var router = express.Router();

// Get method : Call faq view "faq.twig" : /faq
router.get('/', function(req, res, next) {
	res.render('faq', { title: 'FAQ' });
});

module.exports = router;
