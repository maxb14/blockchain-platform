// IMPORTS
var express = require('express');
var ctl = require('../controllers/resultController');
var router = express.Router();

// Get method : Call result controller with the following path : /results
router.get('/', ctl.results);

// Post method : Call blockexplorer controller with the following path : /results
router.post('/', ctl.resultsFile);

module.exports = router;
