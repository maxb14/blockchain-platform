// IMPORTS
var express = require('express');
var ctl = require('../controllers/configController');
var router = express.Router();

// Get method : Call config controller with the following path : /config
router.get('/', ctl.configroot);

// Get method : Call config controller with the following path : /config/blockchainName
router.get('/:name', ctl.configname);

// Post method : Call config controller with the following path : /config/blockchainName
router.post('/:name', ctl.configvalid);

module.exports = router;
