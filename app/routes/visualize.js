// IMPORTS
var express = require('express');
var ctl = require('../controllers/visualizeController');
var router = express.Router();

// Get method : Call visualize controller with the following path : /visualize
router.get('/',ctl.visualizeroot);

// Post method : Call visualize controller with the following path : /visualize
router.post('/', ctl.visualizeroot);

// Post method : Call visualize controller with the following path : /visualize/node
router.post('/node', ctl.visualizeNode);

module.exports = router;
