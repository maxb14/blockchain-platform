// IMPORTS
var express = require('express');
var ctl = require('../controllers/blockexplorerController');
var router = express.Router();

// Get method : Call blockexplorer controller with the following path : /blockexplorer
router.get('/', ctl.runblock);

// Post method : Call blockexplorer controller with the following path : /blockexplorer
router.post('/', ctl.blockresult);

module.exports = router;
