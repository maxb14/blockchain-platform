var elasticsearch = require('elasticsearch');

var elasticClient = new elasticsearch.Client({  
    host: '10.193.203.238:9200',
    log: 'info'
});

var indexName = "scalablockdev";

module.exports = {
	// delete index
	deleteIndex: function() {
		return elasticClient.indices.delete({
			index: indexName
		});
	},
	// cluster health
	clsuterHealth: function() {
		return elasticClient.cluster.health();
	},
	// cluster health
	indexCount: function() {
		return elasticClient.count({
			index: indexName
		});
	},
	// add object to index
	addToIndex: function(id, blockchain, obj) {
		return elasticClient.index({
			index: indexName,
			type: blockchain,
			id: id,
			body: obj
		});
	}
}
