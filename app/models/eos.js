// IMPORTS
var Eos = require('eosjs');
var binaryen = require('binaryen');
var {api, ecc, json, Fcbuffer, format} = Eos.modules;
var urllib = require('urllib');
var fs = require('fs');
var exec = require('child_process').exec;

// Modify if necessary
const eosPath = "/home/max/eos/Docker/eos";
const walletHost = "http://10.193.203.238:8900";


/**
 * model EOS
 */
module.exports = {
	getConfig: function(host) {
		return config = {
			chainId: null, // 32 byte (64 char) hex string
			httpEndpoint: host,
			keyProvider: ['5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3'],
			expireInSeconds: 60,
			broadcast: false,
			verbose: false, // API activity
			sign: true
		};
	},
	// Test connection for an host
	eosHostConnect: function(host) {
		return Eos(this.getConfig(host)).getInfo({});
	},
	// Get block by number
	eosGetBlock: function(host, numblock) {
		return Eos(this.getConfig(host)).getBlock(numblock);
	},
	// Reload old blockchain : re-open wallets
	eosReloadConfig: function(host) {
		exec('cleos wallet list');
		exec('cleos --wallet-url '+walletHost+' wallet unlock -n network --password PW5KUKTkLaokKP9EBQhK5Smeb5uR2pUVeVoWAEs8qdmvHot8JcK3s');
		exec('cleos --wallet-url '+walletHost+' -u '+host+' create account eosio hello.code EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV');
		exec('cleos --wallet-url '+walletHost+' -u '+host+' create account eosio eosio.token EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV');
		//exec('cleos --wallet-url '+walletHost+' -u '+host+' create account eosio eosio.system EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV');
		exec('cleos --wallet-url '+walletHost+' -u '+host+' set contract hello.code '+eosPath+'/build/contracts/hello -p hello.code');
		exec('cleos --wallet-url '+walletHost+' -u '+host+' set contract eosio.token '+eosPath+'/build/contracts/eosio.token -p eosio.token');
		exec('cleos --wallet-url '+walletHost+' -u '+host+' push action eosio.token create \'[\"eosio\", \"1000000000.0000 SYS\", 0, 0, 0]\' -p eosio.token -f');
		exec('cleos --wallet-url '+walletHost+' -u '+host+' push action eosio.token issue \'[\"eosio\", \"1000000.0000 SYS\", \"memo\"]\' -p eosio -f');
		exec('cleos --wallet-url '+walletHost+' -u '+host+' set contract eosio '+eosPath+'/contracts/eosio.bios '+eosPath+'/contracts/eosio.bios/eosio.bios.wast contracts/eosio.bios/eosio.bios.abi');
		//exec('cleos --wallet-url '+walletHost+' -u '+host+' set contract eosio '+eosPath+'/contracts/eosio.system '+eosPath+'/contracts/eosio.system/eosio.system.wast contracts/eosio.system/eosio.system.abi');
	},
	// Create number of accounts
	eosCreateAccounts: function(host, nb) {
		for (i = 1 ; i < nb+1 ; i++) {
			exec('cleos --wallet-url '+walletHost+' -u '+host+' create account eosio user'+i+' EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV');
			exec('cleos --wallet-url '+walletHost+' -u '+host+' push action eosio.token issue \'[\"user'+i+'\", \"1000000.0000 SYS\", \"memo\"]\' -p eosio -f');
		}
	},
	// Push action in blockchain
	eosPushAction: function(host) {
		var action = "cleos --wallet-url "+walletHost+" -u "+host+" push action hello.code hi '[\"eosio\"]' -p eosio -j -f";
		return new Promise(function(resolve, reject) {
			var ex = exec(action, function(err, out, code) {
				if (err) {
					reject(err);
				} else {
					resolve(out);
				}
			});
		});
	},
	// Transfer money from acc1 to acc2
	eosTransferMoney: function(host, acc1, acc2) {
		/*var action = "cleos --wallet-url "+walletHost+" -u "+host+" \
		push action eosio.token transfer \'[\""+acc1+"\", \""+acc2+"\",  \"1.0000 SYS\", \"m\"]\' -p "+acc1+" -j -f";
		
		return new Promise(function(resolve, reject) {
			var ex = exec(action, function(err, out, code) {

			});
		});*/
		return Eos(this.getConfig(host)).transfer(acc1, acc2, "1.0000 SYS", "transfer");
	},
	// Get balance of an account
	eosGetBalanceAccount: function(user) {
		var action = 'cleos get table eosio.token '+user+' accounts';
		return new Promise(function(resolve, reject) {
			var ex = exec(action, function(err, out, code) {
				if (err) {
					reject(err);
				} else {
					resolve(out);
				}
			});
		});
	},
	// Get transaction by ID
	eosGetTransaction: function(host, id) {
		return urllib.request(host+"/v1/account_history/get_transaction", {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: {
				"transaction_id": id
			}
		});
	}
};

