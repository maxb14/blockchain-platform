var urllib = require('urllib');
const { exec } = require('child_process');
const os = require('os');
var Web3 = require('web3');

/**
 * model ETHEREUM
 */
module.exports = {
	// Test connection for an host
	ethHostConnect: function(hostnode) {
		// set the provider you want from Web3.providers
		return new Web3(new Web3.providers.HttpProvider(hostnode));
	},
	// Get all eth accounts
	ethGetAccounts: function(provider) {
		return provider.eth.getAccounts();
	},
	// Get balance of an account
	ethGetBalance: function(provider, account) {
		return provider.eth.getBalance(account);
	},
	// Get balance in ether
	ethGetEthBalance: function(provider, amount) {
		return provider.utils.fromWei(amount, 'ether');
	},	
	// get latest block
	ethGetLatestBlock: function(provider) {
		return provider.eth.getBlock("latest");
	},
	// Execute transaction
	ethPushAction: function(provider, acc1, acc2) {
		return provider.eth.sendTransaction({from:acc1, to:acc2, value: 1000000000000000, gas: 184000});
	}
};
