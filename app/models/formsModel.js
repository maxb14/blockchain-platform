// IMPORTS
var validator = require('validator');

/**
 * FORMS VALIDATION
 */

// Validation model for the config form
exports.configForm = function(req, res, result) {
	var error = null;
	
	if (!validator.isNumeric(result.nodes) || (result.nodes < 1 || result.nodes > 4)) {
		error = "The number of nodes must be between 1 and 4 !";
		req.app.set('nodes', '');
	}
	if (!validator.isNumeric(result.accounts) || (result.accounts < 2 || result.accounts > 5)) {
		error = "The number of accounts must be between 2 and 5 !";
		req.app.set('accounts', '');
	}
	/*if (!validator.isNumeric(result.blocksize) || (result.blocksize < 1 || result.blocksize > 10)) {
		error = "The size of the blocks must be between 1 and 10 mb !";
		req.app.set('blocksize', '');
	}
	if (!validator.isNumeric(result.timeblock) || (result.timeblock < 1 || result.timeblock > 60)) {
		error = "The time between each block must be between 1 and 60 seconds !";
		req.app.set('timeblock', '');
	}*/
	for (key in result) {
		if (validator.isEmpty(result[key])) {
			error = "Please complete all fields !";
		}
	}
	
	return error;
}

// Validation model for the running form
exports.runStart = function(req, res, result) {
	var error = null;

	if (!validator.isNumeric(result.transac) || (result.transac < 1 || result.transac > 100000)) {
		error = "The number of transactions must be between 1 and 100000 !";
		req.app.set('transac', '');
	}
	for (key in result) {
		if (validator.isEmpty(result[key])) {
			error = "Please complete all fields !";
		}
	}
	
	return error;
}

// Validation model for the blockchain explorer form
exports.runBlock = function(req, res, result) {
	var error = null;
	
	if (result.blocknum != '') {
		if (!validator.isNumeric(result.blocknum)) {
			error = "The blocknum must be a number !";
		}
	}
	if (result.blocknum == '' && result.transacid == '') {
		error = "Please complete one field !";
	}
	if (result.blocknum != '' && result.transacid != '') {
		error = "Please complete only one field !";
	}
	
	return error;
}
