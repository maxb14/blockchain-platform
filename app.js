/* Modules */
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var app = express();

/* Determine which blockchain are allowed in platform, add or remove if necessary */
var blockchainsAllowed = ["eos", "ethereum"];

/* Routes definition */
var indexRouter = require('./app/routes/index');
var configRouter = require('./app/routes/config');
var visualizeRouter = require('./app/routes/visualize');
var runRouter = require('./app/routes/run');
var resultsRouter = require('./app/routes/results');
var faqRouter = require('./app/routes/faq');
var blockRouter = require('./app/routes/blockexplorer');

// View engine setup
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'twig');

// Global variables accessible throughout the application
app.set('blockchain', '');
app.set('nodes', '');
app.set('accounts', '');
app.set('blocksize', '');
app.set('timeblock', '');
app.set('networkfile', '');
app.set('transac', '');
app.set('hosts', []);
app.set('blockchainsAllowed', blockchainsAllowed);
app.set('started', false);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/* Path definition */
app.use('/', indexRouter);
app.use('/config', configRouter);
app.use('/visualize', visualizeRouter);
app.use('/run', runRouter);
app.use('/results', resultsRouter);
app.use('/faq', faqRouter);
app.use('/blockexplorer', blockRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
  
});

module.exports = app;
