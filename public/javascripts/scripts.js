// Global variables
var nbNodesMax = 4;
var nbNodesMin = 1;

// Detect text selected and change if necessary
$(document).ready(function(){
    $("#nodes").select(function(e){
		var parent = document.getElementById("nbNodes");
		parent.innerHTML = "";
    });
});

// Execute the function with detection of a change in the input field
$(document).on('input', '#nodes', function(e){
	execute(e.target.value);
});

// If we reload the page with a pre-loaded value
$(document).ready(function(e){
	var input = $("#nodes");
	if (input.length != 0) {
		if ($("#nodes").val() != "") {
			execute($("#nodes").val());
		}
	}
});

// Adds the correct number of host/port fields based on the number of nodes
function execute(value) {
	if (value == "" || value > nbNodesMax || value < nbNodesMin) {
		var parent = document.getElementById("nbNodes");
		parent.innerHTML = "";	
	}
    if (value <= nbNodesMax && value >= nbNodesMin) {
		var port = 8888;
		for (var i = 1 ; i <= value ; i++) {
			var parent = document.getElementById("nbNodes");
			var newInput = document.createElement("input");
			newInput.setAttribute("type", "text");
			newInput.setAttribute("id", "host"+i);
			newInput.setAttribute("value", "http://localhost:"+port);
			newInput.setAttribute("name", "host-"+i);
			var newLabel = document.createElement("label");
			newLabel.setAttribute("for", "host"+i);
			newLabel.appendChild(document.createTextNode("Host/Port : node "+i)); 
			parent.appendChild(newLabel);
			parent.appendChild(newInput);
			port++;
		}
	}	
}

// Detect window resize
window.onresize = function(event) {
	var menu = document.querySelector(".menu");
	var width = $(document).width();
	if(width > 1024) {
		menu.style.display = "block";	
	} else {
		menu.style.display = "none";		
	}
};

// Change responsive menu style (display or not)
function showMenu() {
	var menu = document.querySelector(".menu");
	if (menu.style.display == "none") {
		menu.style.display = "contents";
	} else {
		menu.style.display = "none";
	}
}
